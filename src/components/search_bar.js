/* @flow */

import React, { Component } from 'react';

class SearchBar extends Component {
  state : {
    term: string
  };

  constructor(props: mixed) {
    super(props);

    this.state = {term: ''};
  }

  render() {
    return (
      <div className="form-group search-bar">
        <input
          value={this.state.term}
          onChange={this.onInputChange}
          className="form-control"/>
      </div>
    );
  }

  onInputChange = (event: Event) => {
    if (event.target instanceof HTMLInputElement) {
      this.setState({term: event.target.value});
    }

    this.props.onSearchTermChange(this.state.term);
  }
}

export default SearchBar;
