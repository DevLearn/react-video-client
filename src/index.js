/* @flow */

import _ from 'lodash';
import ReactDOM from 'react-dom';
import React, {Component} from 'react';
import YTSearch from 'youtube-api-search';

import VideoList from './components/video_list';
import SearchBar from './components/search_bar';
import VideoDetail from './components/video_detail';

const YT_API_KEY = 'AIzaSyARZ7ZCNDHOy8pHlBcJeTKOGC3t4opUNdA';

class App extends Component {
    state = {
        videos: [],
        selectedVideo: 0
    };

    constructor(props) {
        super(props);

        this.videoSearch('react.js');
    }

    videoSearch(term) {
        YTSearch({key: YT_API_KEY, term: term}, (videos) => {
            this.setState({
                videos: videos,
                selectedVideo: videos[0]
            });
        });
    }

    render = () => {
        const videoSearch = _.debounce((term) => { this.videoSearch(term)}, 300);

        return (<div className="container">
            <SearchBar onSearchTermChange={videoSearch} />
            <VideoDetail video={this.state.selectedVideo} />
            <VideoList
                onVideoSelect={(selectedVideo) => { this.setState({selectedVideo}) }}
                videos={this.state.videos} />
        </div>);
    }
}

ReactDOM.render(<App/>, document.querySelector(".container"));
